<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Halaman Login </title>
</head>
<body>
	<!-- Heading -->
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<!-- Heading -->

	<!-- Form Input -->
	<form action="/welcome" method="post"> 
        @csrf
		<label>First name:</label>
		<br><br>
		<input type="text" name="first_name">
		<br><br>
		
		<label>Last name:</label>
		<br><br>
		<input type="text" name="last_name">
		<br><br>
		
		<label>Gender:</label>
		<br><br>
		<input type="radio" name="gender">Male <br>
		<input type="radio" name="gender">Female <br>
		<input type="radio" name="gender">Other <br>
		<br>

		<label>Nationality:</label>
		<br><br>
		<select name="nation">
			<option value=""> - Choose - </option>
			<option value="indonesian"> Indonesian </option>
			<option value="Malaysian"> Malaysian </option>
			<option value="Singaporean"> Singaporean </option>
			<option value="Australian"> Australian </option>
		</select>
		<br><br>

		<label>Language Spoken:</label>
		<br><br>
		<input type="checkbox"> Bahasa Indonesia <br>
		<input type="checkbox"> English <br>
		<input type="checkbox"> Other <br>
		<br>

		<label>Bio:</label>
		<br><br>
		<textarea name="bio" cols="50" rows="10"></textarea>
		<br>

		<input type="submit" name="signup" value="Sign UP">
	</form>
	<!-- Form Input -->
</body>
</html>