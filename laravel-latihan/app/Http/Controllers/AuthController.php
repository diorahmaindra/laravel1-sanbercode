<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view('tugas1/form');
    }

    public function signup(Request $request)
    {
       $first_name = $request['first_name'];
       $last_name = $request['last_name'];

       return view('tugas1/welcome', compact('first_name', 'last_name'));
    }
}
